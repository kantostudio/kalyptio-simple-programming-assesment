![Kalyptio Logo](./kalyptio.png "Kalyptio Logo")


# Kalyptio Simple Programming Assessment #

To complete this assessment submit your solution to one of the two following problems in one of the following programming languages:

- Javascript / TypeScript
- Python
- C#
- Java
- Ruby

Do be sure to run all test cases and compare with the expected results prior to submission.

Bonus Points For:

- Unit/Functional tests
- Useful comments
- Commit messages (include .git in zip)
- Performant code

---

### 1. Inversion Count ###
This is a classic programming exercise but whe are interested in learning how you would solve this problem in the most performant way possible.

Let `A[0...n - 1]` be an array of n distinct positive integers placed in some arbitrary order. If `i < j` and `A[i] > A[j]` then the pair `(i, j)` is called an inversion of `A`. Given `n` and an array `A` your task is to **find the number of inversions of `A`**.

#### Input ####
The first line contains `t`, the number of testcases followed by a blank space. Each of the `t` tests start with a number `n` where `(n <= 200000)`. Then `n + 1` lines follow. In the `i-th` line a number `A[i - 1]` is given (where `A[i - 1] <= 10^7`). The `(n + 1)th` line is a blank space.

#### Output ####
For every single test **output exactly one line** giving the number of **inversions of `A`**.

#### Example ####
```
Input:
2

3
3
1
2

5
2
3
8
6
1


Output:
2
5
```
---

### 2. Alien Class Leader ###
Aliens in the planet Utapau are looking to determine who will be their next class leader. To do this they have invented a very unique game:

These are the ways how the game is played.
1. There are `n` students in the class. Each student is labeled from `1` (first student) to `n` (last student).

2. A paper is given to the `m-th` student.

3. The next `o-th` student who gets the paper leaves the game.

4. The paper is passed until there is one last student who hasn't left the game.

5. That student will then become the class leader. Yay for that sudent!

Now, your task is to **find the number of the** remaining student, the **class leader**.

#### Input ####
The first line contains a number `T` where `(0 <= T <= 106)`.
Each of the next `T` lines contains `3` integers which are `n (0 < n <= 103)`, `m`, `o (0 < m, o <= n)` and are separated by a single space.

#### Output ####
For each test cases, print the required answer.

#### Example ####

```
Input:
2
4 1 2
5 2 3

Output:
2
1
```

**Explanation for test case 1**

1. 1 2 3 4 -> The paper is being held by student 1.
 Pass the paper by 2 students. Now, the paper is being held by student 3.
2. 1 2 4    -> Student 3 quits. Pass the paper by 2 students. Now, the paper is being held by student 1.
3. 2 4       -> Student 1 quits. Pass the paper by 2 students. Now, the paper is being held by student 4.
4. 2          -> Student 4 quits. Student 2 becomes the class leader.